import java.util.Iterator;
import java.util.Random;

// En liste der vi kan "deaktivere" det som er på et gitt sted.
// Det gjør lista mindre, og tingen kan flyttes tilbake ved å
// "reaktivere".

// Why oh why can't I just be permitted to do this?

// list<T>::iterator pos = our_list.begin();
//
// while (pos != our_list.end()) {
// 	T instance = *pos;
// 	pos = our_list.erase(pos);
// 	do_something(instance);
// 	recurse(our_list);
// 	pos.insert(instance);	
// }

class Node<T> {
	public Node<T> forrige;
	public Node<T> neste;

	public T innhold;
}

class DeaktiveringsListe<T> implements Iterable<T> {

	private Node<T> hode;
	private Node<T> sist;
	int lengde;

	DeaktiveringsListe() {
		lengde = 0;
		hode = new Node<T>();
		hode.innhold = null; // Sentinel
		hode.forrige = null;
		hode.neste = null;
		sist = hode;
	}

	public Node<T> forste() {
		return (hode.neste);
	}

	public Node<T> neste(Node<T> naaverende) {
		return(naaverende.neste);
	}

	public boolean erSiste(Node<T> node) {
		return (node.neste == null);
	}

	public void leggTilNodeEtter(Node<T> etterNode, Node<T> leggTil) {
		leggTil.forrige = etterNode;
		leggTil.neste = etterNode.neste;
		etterNode.neste = leggTil;

		if (leggTil.neste != null)
			leggTil.neste.forrige = leggTil;
		
		if (sist == etterNode)
			sist = leggTil;

		++lengde;
	}

	public void leggTilEtter(Node<T> etterNode, T leggTil) {
		Node<T> ny = new Node<T>();
		ny.innhold = leggTil;
		leggTilNodeEtter(etterNode, ny);
	}

	public void leggTilForst(T leggTil) {
		leggTilEtter(hode, leggTil);
	}

	public void leggTilSist(T leggTil) {
		leggTilEtter(sist, leggTil);
	}

	public void fjern(Node<T> skalFjernes) {
		if (skalFjernes.forrige != null)
			skalFjernes.forrige.neste = skalFjernes.neste;
		
		if (skalFjernes.neste != null)
			skalFjernes.neste.forrige = skalFjernes.forrige;
		
		if (sist == skalFjernes)
			sist = skalFjernes.forrige;

		--lengde;
	}

	// Antar at ingen har fikla med lista i mellomtiden.
	public void leggTilbake(Node<T> gjeninnfores) {
		if (gjeninnfores.forrige == null)
			throw new IllegalArgumentException("ErstattIgjen med hode???");

		leggTilNodeEtter(gjeninnfores.forrige, gjeninnfores);
	}

	// flytt noden ett hakk fram
	public void leggTilbakeEnFram(Node<T> gjeninnfores) {
		if (gjeninnfores.forrige == null || gjeninnfores.forrige.forrige == null)
			leggTilbake(gjeninnfores);
		else
			leggTilNodeEtter(gjeninnfores.forrige.forrige, gjeninnfores);
	}

	public int len() {
		return (lengde);
	}

	// NB: Hvis du fikler med fjern/erstattIgjen mens du er inni 
	// iteratoren, kan det lett si pang. 
	public Iterator<T> iterator() {
		class denneIterator implements Iterator<T> {
			private Node<T> curNode;

			denneIterator() {
				curNode = hode;
			}

			public boolean hasNext() {
				return (curNode.neste != null);
			}

			public T next() {
				curNode = curNode.neste;
				return(curNode.innhold);
			}

			public void remove() {
				if (curNode == hode)
					throw new IllegalStateException();

				Node<T> skalFjernes = curNode;
				curNode = curNode.neste;

				fjern(skalFjernes);
			}
		};

		return(new denneIterator());
	}
}

class ListeTest {
	Random rnd;

	public void recursive(int idx, DeaktiveringsListe<String> test) {
		if (test.len() == 0) return;

		// Fjern noden som tilsvarer order[idx] % test.len().
		int fjernIdx = rnd.nextInt(test.len());
		if (fjernIdx < 0) fjernIdx = -fjernIdx;

		Node<String> backup = test.forste();

		for (int i = 0; i < fjernIdx; ++i)
			backup = test.neste(backup);
		test.fjern(backup);

		int faktiskLen = 0;

		for (String s: test) {
			System.out.print(s + "\t");
			++faktiskLen;
		}
		System.out.println();
		if (faktiskLen != test.len())
			throw new IllegalStateException();

		// Kall selv med idx+1
		recursive(idx+1, test);
		// Flytt noden tilbake.
		test.leggTilbake(backup);
	}

	public void test() {
		int[] order = new int[20];
		rnd = new Random();

		// Først sett inn en del elementer

		DeaktiveringsListe<String> foo = new DeaktiveringsListe<String>();

		int i;
		for (i = 0; i < 40; ++i) {
			foo.leggTilSist(new Integer(i).toString()); // No commen'.
		}

		for (i = 0; i < order.length; ++i) {
			order[i] = rnd.nextInt(1000);
		}

		for (String s: foo)
			System.out.print(s + "\t");
		System.out.println();

		recursive(0, foo);

		System.out.println("\n");

		for (String s: foo)
			System.out.print(s + "\t");
		System.out.println();
	}
}

/*
class TestRun {
	public static void main(String[] args) {
		new ListeTest().test();
	}
}*/