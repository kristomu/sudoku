import java.util.*;

class Fuzzer {

	//public Brett originalSudokuBrett;

	// Lagre det i et format: (x, y, tall). Det blir sortert i en liste, randomly
	// permuted, og så leser vi av bare de n siste. (Grunnen til å ha dette er at
	// jeg tror det kan bli brukt i videre funksjonalitet.)

	class VerdiEnhet {
		public int x, y, verdi;
	}

	public ArrayList<VerdiEnhet> satteElementer;
	public int boksX, boksY;

	public void lesAvBrett(Brett brettet) {

		satteElementer = new ArrayList<VerdiEnhet>();
		boksX = brettet.giBoksXLengde();
		boksY = brettet.giBoksYLengde();

		for (int y = 0; y < brettet.giMaksverdi(); ++y) {
			for (int x = 0; x < brettet.giMaksverdi(); ++x) {
				if (brettet.harVerdi(x, y)) {
					VerdiEnhet denne = new VerdiEnhet();
					denne.x = x;
					denne.y = y;
					denne.verdi = brettet.giVerdi(x, y);
					satteElementer.add(denne);
				}
			}
		}
	}

	public Brett lagFuzzaBrett(int min, int maks) {
		Brett nyttBrett = new Brett(boksX, boksY);

		Collections.shuffle(satteElementer);

		int lesSaaLangtInn = -1;
		while (lesSaaLangtInn < 0 || lesSaaLangtInn <= min ||
			lesSaaLangtInn >= maks)

			lesSaaLangtInn = satteElementer.size() - 
				new Random().nextInt(satteElementer.size()/3);

		for (int i = 0; i < lesSaaLangtInn; ++i) {
			VerdiEnhet denne = satteElementer.get(i);
			nyttBrett.settVerdi(denne.x, denne.y, denne.verdi);
		}

		return(nyttBrett);
	}

	public Brett lagFuzzaBrett() {
		return (lagFuzzaBrett(-1, satteElementer.size()+1));
	}

	public void test(Brett inn) {
		lesAvBrett(inn);

		String brettString = SudokuSerialisering.skrivBrettTilUtvidetString(inn);
		Brett test = SudokuSerialisering.lesBrettFraUtvidetString(brettString);
		
		// Løs test en gang for å finne antall løsninger.
		// TODO: finn en måte å gjeninnføre "abort if two".

		SudokuBeholder losninger = new SudokuBeholder(750);
		long antLosninger = test.fyllUt(losninger);

		System.out.println("Her skal det være " + antLosninger + " løsninger.");

		for (int i = 0; i < 1000; ++i) {
			test = lagFuzzaBrett();
			test.settMaksOnskedeLosninger(antLosninger+1);
			losninger = new SudokuBeholder(750);

			long losningerHer = test.fyllUt(losninger);

			System.out.print(losningerHer + " ");

			if (losningerHer < antLosninger) {
				System.out.println("\n Noe er galt!");
			}
		}
	}

	public Brett finnNyttBrett(Brett lostBrett, int minLosninger, 
		int maksLosninger) {

		// Fuzz det løste brettet 20 ganger eller noe slikt. Velg løsningen som har
		// færrest satte verdier og ligger mellom min og maks.
		// TODO?: bool BareOptimalisering

		lesAvBrett(lostBrett);
		String brettString = SudokuSerialisering.skrivBrettTilUtvidetString(lostBrett);
		//System.out.println(brettString);
		Brett test = SudokuSerialisering.lesBrettFraUtvidetString(brettString);

		String rekordString = "";
		int rekordAntallMedVerdi = test.giAntallMedVerdi();
		int minlimit = 0;

		Collections.shuffle(satteElementer);

		int forsok = 0;

		for (int i = 0; i < 200 && forsok < 30; ++i) {
			Brett nyttBrett = new Brett(lostBrett.giBoksXLengde(), 
				lostBrett.giBoksYLengde());

			for (int j = 0; j < satteElementer.size() - i; ++j) {
				VerdiEnhet denne = satteElementer.get(j);
				nyttBrett.settVerdi(denne.x, denne.y, denne.verdi);
			}

			String muligRekord = SudokuSerialisering.skrivBrettTilUtvidetString(nyttBrett);

			SudokuBeholder losninger = new SudokuBeholder(maksLosninger);
			nyttBrett.settMaksOnskedeLosninger(maksLosninger+1);

			long losningerHer = nyttBrett.fyllUt(losninger);
			//System.out.print(losningerHer + " ");

			if (losningerHer > maksLosninger) {
				Collections.shuffle(satteElementer);
				++forsok;
				--i;
				continue;
			}

			if (losningerHer >= minLosninger && losningerHer <= maksLosninger) {
				forsok = 0;
				rekordString = muligRekord;
				rekordAntallMedVerdi = test.giAntallMedVerdi();
				System.out.print(losningerHer + " ");
			}
		}

		System.out.println("");

		if (rekordString.equals(""))
			return(null);
		else
			return(SudokuSerialisering.lesBrettFraUtvidetString(rekordString));
	}

	public Brett finnNyttBrett(int xStorrelse, int yStorrelse, int minLosninger, 
		int maksLosninger, String navn) {

		Brett skalFylles = new Brett(xStorrelse, yStorrelse);
		SudokuBeholder loser = new SudokuBeholder(2);
		skalFylles.settMaksOnskedeLosninger(1);

		// Prøv å sette navnet langs første rad. Hiver exception hvis det ikke går.
		for (int x = 0; x < navn.length(); ++x) {
			char curVerdi = navn.toUpperCase().charAt(x); 
			if (!skalFylles.settFraPrintbarVerdi(x, 0, curVerdi, false)) {
				throw new IllegalArgumentException("Kunne ikke lagre navn");
			}
		}

		skalFylles.fyllUt(loser, true); // I tilfeldig rekkefølge -> tilfeldig brett
		skalFylles = loser.taUt(0);

		return(finnNyttBrett(skalFylles, minLosninger, maksLosninger));
	}

	public Brett finnNyttBrett(int xStorrelse, int yStorrelse, int minLosninger, 
		int maksLosninger) {

		return (finnNyttBrett(xStorrelse, yStorrelse, minLosninger, maksLosninger,
			""));
	}
}