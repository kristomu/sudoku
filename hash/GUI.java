// This is XXX YYY. XXX YYY is XXX!
// TODO? Fiks GUI-resizing-greier.

import java.io.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.border.*;

class Layout {

	private static MatteBorder giKant(int x, int y, int boksXStorrelse, 
		int boksYStorrelse) {

		int elementer = boksXStorrelse * boksYStorrelse;

		// Uansett hvilken rute det er, så trenger vi en vannrett og en 
		// loddrett linje for å skille rutene fra hverandre.
		int topp = 0, venstre = 0, hoyre = 1, bunn = 1;

		// Hvis vi er ved et skille mellom bokser, så tegn en tykkere linje.

		if (x % boksXStorrelse == boksXStorrelse - 1)
			hoyre = 2;
		if (y % boksYStorrelse == boksYStorrelse - 1)
			bunn = 2;

		// Hvis ruta vi skal tegne er i kanten, må vi tegne en tykk linje
		// i den retningen som kanten er (dvs ruta helt til venstre må
		// ha en linje til venstre osv.)

		if (x == 0)
			venstre = 2;
		
		if (y == 0)
			topp = 2;
		
		if (x == elementer-1)
			hoyre = 2;
		
		if (y == elementer-1)
			bunn = 2;

		return (BorderFactory.createMatteBorder(topp, venstre, bunn, hoyre,
			Color.black));
	}

	public static void leggTilSudoku(JPanel brettDisplay, Brett innBrett) {

		int elementer = innBrett.giMaksverdi();

		brettDisplay.setLayout(new GridLayout(elementer, elementer));

		for (int y = 0; y < elementer; ++y)
			for (int x = 0; x < elementer; ++x) {
				char verdiHer = innBrett.giPrintbarVerdi(x, y);

				// Funksjonell programmering:

				// (* 2 (+ 1 2))

				// Java "enterprise/API" style (opa Java style!):

				// new MathematicalOperator().execute(MP_MULTIPLY, 
				//  new OperandPair(2, new MathematicalOperator().execute(MP_ADD,
				//    new OperandPair(1, 2)).toInt())).toInt();

				// Jeg overdriver, but seriously?

				String innhold = "";
				if (verdiHer != '.')
					// Fordi "string(verdiHer)" ville vært for enkelt og kortfattet.
					innhold = new Character(verdiHer).toString();

				JLabel rute = new JLabel(innhold, SwingConstants.CENTER);
				rute.setPreferredSize(new Dimension(22, 24));
				rute.setBorder(giKant(x, y, innBrett.giBoksXLengde(), 
					innBrett.giBoksYLengde()));
				rute.setOpaque(true);

				int boksy = (y / innBrett.giBoksYLengde());
				int boksx = (x/innBrett.giBoksXLengde());

				// Annenhver er hvit og grå.
				if ((boksy + boksx) % 2 == 0)
					rute.setBackground(Color.white);
				else
					rute.setBackground(new Color(0xE8, 0xE8, 0xE8));

				rute.setAlignmentX(JLabel.CENTER_ALIGNMENT);
				brettDisplay.add(rute);
			}
	}

	public static JPanel giSudoku(Brett innBrett) {
		JPanel brettDisplay = new JPanel();

		leggTilSudoku(brettDisplay, innBrett);
		return (brettDisplay);
	}

	public JButton leggTilKnapp(Container leggTilHvor, String innhold, int padding,
		ActionListener hvaSkjer, boolean skruddPaa) {

		JButton knapp = new JButton(innhold);
		knapp.setAlignmentX(Component.LEFT_ALIGNMENT);
		if (hvaSkjer != null)
			knapp.addActionListener(hvaSkjer);

		knapp.setEnabled(skruddPaa);
		leggTilHvor.add(knapp);

		if (padding > 0)
			leggTilHvor.add(Box.createHorizontalStrut(padding));

		return (knapp);
	}

	public void leggTilMellomrom(Container leggTilHvor, int avstand) {
		leggTilHvor.add(Box.createVerticalStrut(avstand));
	}

}

class BrettGUI {
	
	JFrame hovedramme;
	JFileChooser filvalg, lagrevalg;

	JPanel topp, bunn, sudoku;
	JLabel losninger;
	Layout layout;

	Brett sudokuBrett;

	JButton losningsKnapp, nesteLosning, forrigeLosning, aapneKnapp, nyKnapp, lagreKnapp;

	LosningsTraad loser;

	// for å vise løsninger
	boolean holderPaaAaLose;
	SudokuBeholder sbLosninger;
	int vistLosningNr, gammelVistLosningNr;

	BrettGUI() {
		hovedramme = new JFrame("Sudokuprogram");
		layout = new Layout(); 

		filvalg = new JFileChooser();

		String sti = System.getProperty("user.dir"); // eller java.nio.file.Paths.get("");
		filvalg.setCurrentDirectory(new java.io.File(sti));

		vistLosningNr = -1;
		gammelVistLosningNr = -1;
		holderPaaAaLose = false;

		loser = new LosningsTraad();
	}

	// --- ACTION LISTENER og lignende logikk ---

	class LosningsTraad extends Thread {

		public void run() {
			losningsKnapp.setEnabled(false);
			// Kludge siden vi ikke kan bruke Thread.stop()
			// Fiks hvis det er tid.
			aapneKnapp.setEnabled(false);
			nyKnapp.setEnabled(false);

			sbLosninger = new SudokuBeholder(750);
			holderPaaAaLose = true;
			sudokuBrett.fyllUt(sbLosninger); // Magic happens here.
			holderPaaAaLose = false;

			// Bug i multithreaderen gjør at det kan si pang
			// når man prøver å løse flere brett etter hverandre.
			// Fikser det slik. Bedre fiks hvis jeg får tid...
			//aapneKnapp.setEnabled(true);
			oppdaterLosningDisplay();
		}
	}

	public ActionListener avsluttActionListener() {
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) { System.exit(0); }};
	}

	public ActionListener nyActionListener() {
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) { ny(); }};
	}

	public ActionListener aapneActionListener() {
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) { aapne(); }};
	}

	public ActionListener lagreActionListener() {
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) { lagre(); }};
	}

	public ActionListener losActionListener() {
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) { loser.start(); }};
	}

	public ActionListener nesteActionListener() {
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) { flyttLosningNr(1); }};
	}

	public ActionListener forrigeActionListener() {
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) { flyttLosningNr(-1); }};
	}

	public void aapne() {
    	if(filvalg.showOpenDialog(hovedramme) != JFileChooser.APPROVE_OPTION)
    		return;

    	File fil = filvalg.getSelectedFile();
    	if (fil == null || fil.getName().equals("")) return;

   		try {
   			erstattBrett(OSudokuSerialisering.lesBrettFraFil(fil), true);
   		} catch (FileNotFoundException e) {
   			JOptionPane.showMessageDialog(hovedramme, "Fant ikke fila.",
   				"Filfeil", JOptionPane.ERROR_MESSAGE);
   		} catch (IOException e) {
   			JOptionPane.showMessageDialog(hovedramme, "Feil ved lesing av fil!",
   				"Filfeil", JOptionPane.ERROR_MESSAGE);
   		} catch (NumberFormatException e) {
   			JOptionPane.showMessageDialog(hovedramme, "Feil filformat. Dette ser ikke ut som en Sudoku-fil!",
   				"Filfeil", JOptionPane.ERROR_MESSAGE);
   		}
	}

	public void lagre() {
		if(filvalg.showSaveDialog(hovedramme) != JFileChooser.APPROVE_OPTION)
    		return;

    	File fil = filvalg.getSelectedFile();
    	if (fil == null || fil.getName().equals("")) return;

    	try {
			PrintStream utFilStream = new PrintStream(fil);
			OSudokuSerialisering.skrivBrett(sudokuBrett, utFilStream);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(hovedramme, "Problemer med å skrive til fila!",
   				"Filfeil", JOptionPane.ERROR_MESSAGE);
			return;
		}
	}	

	private boolean gjentarseg(String x) {

		for (int i = 0; i < x.length(); ++i)
			for (int j = 0; j < i; ++j)
				if (x.charAt(i) == x.charAt(j) && i != j)
					return(true);

		return(false);
	}

	public void ny() {
		// Tidspress!
		System.out.println("Vennligst vent. GUIen kan henge en liten stund.");
		losninger.revalidate();

		String rader = JOptionPane.showInputDialog("Hvor mange rader skal en boks ha?");
		String kolonner = JOptionPane.showInputDialog("Hvor mange kolonner skal en boks ha?");
		String kultord = JOptionPane.showInputDialog(
			"<HTML>Vil du skjule navnet ditt, et kult ord eller en tallsekvens i "+
			"Sudokuen?<BR>Skriv det her (f.eks CAFE for 4x4).<BR>Merk at du ikke kan bruke bokstaver " + 
			"som ikke er tillatt i Sudokuen,<BR>eller bruke en bokstav flere ganger.");
		kultord.replaceAll("\\s+","");

		// Fjern mellomrom

		int xStorrelse = sudokuBrett.giBoksXLengde();
		int yStorrelse = sudokuBrett.giBoksYLengde();

		int kandidatX = -1, kandidatY = -1;

		try {
			kandidatX = Integer.parseInt(rader);
			kandidatY = Integer.parseInt(kolonner);

			if (kandidatX * kandidatY <= 0 || kandidatX * kandidatY > 36 || kandidatX <= 0 || kandidatY <= 0)
				throw new NumberFormatException();

			System.out.println(kandidatX + " " + kandidatY);

			xStorrelse = kandidatX;
			yStorrelse = kandidatY;

		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, 
				"<html>Det er dessverre ikke en gyldig st&oslash;rrelse." + 
				"<br>Bruker brettets st&oslash;rrelse isteden.</html>",
				"Beklager", JOptionPane.INFORMATION_MESSAGE);
		}

		if (gjentarseg(kultord.toUpperCase())) {
			JOptionPane.showMessageDialog(null, 
				"<html>Det kule ordet inneholder samme bokstav mer enn en gang." + 
				"<br>Kan derfor ikke bruke det! Avbryter.</html>",
				"Beklager", JOptionPane.INFORMATION_MESSAGE);
			return;
		}

		JOptionPane.showMessageDialog(null, "<html>Trykk OK for &aring starte.<BR>Se konsoll for videre info.</html>",
				"Info", JOptionPane.INFORMATION_MESSAGE);

		System.out.println("Begynner med genereringsjobben naa...");

		Fuzzer generator = new Fuzzer();
		Brett nyttBrett = null;
		try {
			nyttBrett = generator.finnNyttBrett(xStorrelse,
				yStorrelse, 1, 1, kultord);
		} catch (IllegalArgumentException e) {
			JOptionPane.showMessageDialog(null, "<html>Klarte ikke &aring; legge "
			+ "inn det kule ordet.<br>Lager et vanlig brett isteden!</html>",
				"Beklager", JOptionPane.INFORMATION_MESSAGE);
			nyttBrett = generator.finnNyttBrett(xStorrelse, yStorrelse,
				1, 1);
		}

		System.out.println("Ferdig!");

		if (nyttBrett == null) {
			JOptionPane.showMessageDialog(null, "Fant dessverre ikke noe nytt brett.",
				"Beklager", JOptionPane.INFORMATION_MESSAGE);
		} else {
			System.out.println("Brettet er: ");
			OSudokuSerialisering.skrivBrett(nyttBrett, System.out);
			erstattBrett(nyttBrett, true);
		}
	}

	public void flyttLosningNr(int delta) {
		if (sbLosninger == null) return;

		int antLagredeLosninger = sbLosninger.hentAntallLosninger();
		int onsketLosningNr = vistLosningNr + delta;

		if (onsketLosningNr < 0 || onsketLosningNr >= antLagredeLosninger)
			return;

		vistLosningNr = onsketLosningNr;
		oppdaterLosningDisplay();
	}

	// --- End ---

	// Oppdatering av GUIet

	private void autoResize() {

		// Gjør om størrelsen hvis den er for liten.

		if (hovedramme.getSize().height < hovedramme.getPreferredSize().height ||
			hovedramme.getSize().width < hovedramme.getPreferredSize().width) {

			hovedramme.pack();
			losninger.revalidate();
		}
	}

	public synchronized void oppdaterLosningDisplay() {
		
		if (sbLosninger == null) {
			losninger.setText("<html>Har ikke l&oslash;st brettet " +
				"enn&aring;.</html>");
			losninger.revalidate();
			nesteLosning.setEnabled(false);
			forrigeLosning.setEnabled(false);
			losningsKnapp.setEnabled(true);
			autoResize();
			return;
		} else {
			losningsKnapp.setEnabled(false);
		}

		// Hvis vi har minst en løsning og vistLosningNr er -1, så sett det til 0
		// slik at første løsning automatisk blir vist.

		int antLagredeLosninger = sbLosninger.hentAntallLosninger();

		if (vistLosningNr < 0 && antLagredeLosninger > 0)
				vistLosningNr = 0;

		// Sjekk løsningsnr.
		// Hvis det ikke er det samme som det gamle
		// 		oppdater løsningen ved å skrive løsningsbrettet ut til skjermen.

		if (vistLosningNr != gammelVistLosningNr) {
			gammelVistLosningNr = vistLosningNr;
			Brett lostBrett = sbLosninger.taUt(vistLosningNr);

			if (lostBrett != null)
				erstattBrett(lostBrett, false);
		}

		long antLosninger = sbLosninger.hentAntallTotaleLosninger();

		// skru av/på gråing av neste/forrige hvis nødvendig
		nesteLosning.setEnabled (vistLosningNr < antLagredeLosninger-1);
		forrigeLosning.setEnabled(vistLosningNr > 0);

		// skriv løsningsnr og antall løsninger til skjermen
		String status = "Brettet er l&oslash;st.";
		String suffix = "";

		if (holderPaaAaLose) {
			status = "Holder p&aring; &aring; l&oslash;se...";
			suffix = " hittil";
		}

		if (antLosninger == 0)
			losninger.setText("<html>" + status + "<br>Ingen l&oslash;sninger"
				+ suffix + ".</html>");
		else if (antLosninger == 1)
			losninger.setText("<html>" + status + "<br>1 l&oslash;sning" + 
				suffix +".</html>");
		else
			losninger.setText(String.format(new java.util.Locale("no"), 
				"<html><center>%s<br>%,d l&oslash;sninger" + suffix 
				+".<br>Viser nr. %d av %d</center></html>", status, 
				antLosninger, vistLosningNr+1, antLagredeLosninger));

		// Resize hvis den nå er for liten.
		autoResize();
	}


	private JPanel giTopp() {
		JPanel rad = new JPanel();
		rad.setLayout(new BoxLayout(rad, BoxLayout.X_AXIS));

		int avstand = 10;

		losningsKnapp = layout.leggTilKnapp(rad, "<html>L&oslash;s</html>", 
			avstand, losActionListener(), true);
		nesteLosning = layout.leggTilKnapp(rad, "Neste", avstand,  
			nesteActionListener(), false);
		forrigeLosning = layout.leggTilKnapp(rad, "Forrige", avstand,
			forrigeActionListener(), false);

		return(rad);
	}

	private JPanel giBunn() {
		// Duplisert kode for svingende...
		JPanel knapperad = new JPanel();
		knapperad.setLayout(new BoxLayout(knapperad, BoxLayout.X_AXIS));

		int avstand = 10;

		nyKnapp = layout.leggTilKnapp(knapperad, "<html>Ny</html>", 
			avstand, nyActionListener(), true);
		aapneKnapp = layout.leggTilKnapp(knapperad, "<html>&Aring;pne</html>", 
			avstand, aapneActionListener(), true);
		lagreKnapp = layout.leggTilKnapp(knapperad, "<html>Lagre</html>", 
			avstand, lagreActionListener(), true);
		layout.leggTilKnapp(knapperad, "Avslutt", avstand, 
			avsluttActionListener(), true);

		return(knapperad);
	}

	public void kjorGUI(Brett innBrett) {
		hovedramme.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		hovedramme.setLayout(new BoxLayout(hovedramme.getContentPane(), 
			BoxLayout.Y_AXIS));

		// Sett standardfonter.
		UIManager.put("Label.font", 
			new javax.swing.plaf.FontUIResource("Sans Serif",Font.PLAIN,19));
		UIManager.put("Button.font", 
			new javax.swing.plaf.FontUIResource("Sans Serif",Font.PLAIN,16));

        // Legg til innhold.
        hovedramme.add(giTopp());
        layout.leggTilMellomrom(hovedramme, 10);
        sudokuBrett = innBrett;
        sudoku = Layout.giSudoku(innBrett);
		hovedramme.add(sudoku);
        layout.leggTilMellomrom(hovedramme, 10);

        JPanel statusramme = new JPanel();
        statusramme.setLayout(new BoxLayout(statusramme, BoxLayout.X_AXIS));
        losninger = new JLabel("", SwingConstants.CENTER);
		statusramme.add(losninger);
		hovedramme.add(statusramme);
		layout.leggTilMellomrom(hovedramme, 10);

        hovedramme.add(giBunn());

        oppdaterLosningDisplay();
 
        // Vis vinduet.
        hovedramme.pack();
        hovedramme.setVisible(true);
	}

	// heltNytt er true hvis brettet ikke er en løsning til det tidligere brettet,
	// dvs at vi skal fjerne løsninger og sette løsningstelleren til -1 i tillegg.
	public void erstattBrett(Brett nyttBrett, boolean heltNytt) {
		sudokuBrett = nyttBrett;

		sudoku.removeAll();
		Layout.leggTilSudoku(sudoku, sudokuBrett);

		if (heltNytt) {
			sbLosninger = null;
			vistLosningNr = -1;
			gammelVistLosningNr = -1;
			oppdaterLosningDisplay();
		}

		hovedramme.invalidate();
		sudoku.revalidate();
		hovedramme.repaint();
	}
}

class Sammenkobling implements Runnable {

	BrettGUI gui;

	// Begynnelsen på GUI-tråden
	public void run() {
    	gui = new BrettGUI();
        gui.kjorGUI(new Brett(3, 3));
	}

	public void main() {
		// Sett opp GUI-tråden og kjør run() på den
		javax.swing.SwingUtilities.invokeLater(this);

		// Jevnlig oppdatere antall løsninger.
		for(;;) {
			try {
				Thread.sleep(100);

				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() { gui.oppdaterLosningDisplay(); }});
			} catch (InterruptedException e) {}
		}
	}
}