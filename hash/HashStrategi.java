import java.util.*;

// Hashstrategi

// Strategien bak hashing er at når vi har 750 (eller hva det nå er) brett, så
// trenger vi ikke lagre flere, bare finne ut hvor mange det er. Så hvis vi kommer
// til en situasjon hvor begrensningene på alle rader, kolonner og bokser er lik
// noe vi har sett før, så vet vi allerede hvor mange løsninger man kan få ved å
// gå nedover i den retningen. Da kan vi bare slå opp i en cache hvor mange brett
// det er og så returnere det tidligere.

// Klassen inneholder en hashliste og håndterer også inkrementell hashing til en
// array, dvs. at hashen blir oppdatert etterhvert som spillet går i stedet for at
// man trenger å regne den ut "fra børjan" hver eneste gang. (Se Zobrist hashing).

// Hver index i arrayen inneholder en brettrepresentasjon (siden hashen kan
// kollidere) og et tall som sier hvor mange brett som ligger nedover der.

class HashStrategi {

	class HashInnhold {
        public long[] begrensninger;
        long antBrett;
	}

    private ArrayList<HashInnhold> cache;
    private int antallTTabellElementer;

    private ZobristIntBeholder fellesHash;

    public int[] dybdeCounter;
    private int maksDybde;

    ArrayList<RepresentasjonsTall> representasjonerReferanser;

    // Er dette hackish?
    int vedDybde;

    // Hvis bitstorrelse er b, så er antall elementer maks 2^b-1

    private void leggTilGruppeRef(SudokuGruppe[] grupper) {
        for (SudokuGruppe g: grupper)
            representasjonerReferanser.add(g.giRepresentasjonObjekt());
    }

    private boolean erRepLik(long[] likHva) {
        for (int i = 0; i < representasjonerReferanser.size(); ++i)
            if (likHva[i] != representasjonerReferanser.get(i).verdi)
                return(false);

        return(true);
    }

    public boolean skalLeggeInnHash(int dybde) {
        return(dybde < maksDybde);
    }

    private long[] representasjonerTilLong(ArrayList<RepresentasjonsTall> inn) {
        long[] retval = new long[inn.size()];

        int i = 0;
        for(RepresentasjonsTall r: inn) {
            retval[i++] = r.verdi;
          //  System.out.print(r.verdi + " ");
        }
        //System.out.println("");

        return(retval);
    }

    HashStrategi(int bitstorrelse, int maksDybdeInn) {

    	antallTTabellElementer = (1 << bitstorrelse);
        cache = null;
    	//cache = new ArrayList<HashInnhold>(antallTTabellElementer);
        fellesHash = new ZobristIntBeholder();
        maksDybde = maksDybdeInn;
        dybdeCounter = new int[maksDybde];

        vedDybde = -1;
        // Legg så inn referansene fra sudokugruppene i representasjonerReferanser.
        // Når gruppene forandrer på innholdet kan vi lese det rett av.
        representasjonerReferanser = new ArrayList<RepresentasjonsTall>();
    }

    public void settGruppeReferanser(SudokuGruppe[] rader, 
        SudokuGruppe[] kolonner, SudokuGruppe[] bokser) {
        leggTilGruppeRef(rader);
        leggTilGruppeRef(bokser);
        leggTilGruppeRef(kolonner);
    }

    public ZobristIntBeholder giFellesHash() {
        return (fellesHash);
    }

    public int giForkortetHash() {
        return (fellesHash.hash & (antallTTabellElementer-1));
    }

    public long giCachetAntallBrett() {
        int forkortetHash = giForkortetHash();

        if (cache != null && cache.size() > forkortetHash && 
                cache.get(forkortetHash) != null &&
                cache.get(forkortetHash).begrensninger != null && 
                erRepLik(cache.get(forkortetHash).begrensninger))
                //representasjonerReferanser.equals(cache.get(forkortetHash).begrensninger))
                //.equals(representasjonerReferanser))
            return (cache.get(forkortetHash).antBrett);

        return(-1);
    }

    public void leggTilCache(long[] representasjon, long antBrett) {
        // Hvis det ikke er nok elementer, legg til...
        // Kanskje vi skulle bruke HashMap i stedet?
        if (cache == null)
            cache = new ArrayList<HashInnhold>(antallTTabellElementer);

        int maksHittil = cache.size();

        for (int i = maksHittil; i <= giForkortetHash(); ++i)
            cache.add(null);

        HashInnhold detteInnhold = cache.get(giForkortetHash());
        boolean varNull = (detteInnhold == null);

        if (varNull)
            detteInnhold = new HashInnhold();

        detteInnhold.begrensninger = representasjon;
        detteInnhold.antBrett = antBrett;

        if (varNull)
            cache.set(giForkortetHash(), detteInnhold);
    }

    public long sjekkForHash(int dybde, Brett brettet) {
        if (dybde >= maksDybde) return(-1);
        vedDybde = dybde;

        long cachetAntallBrett = giCachetAntallBrett();

        if (cachetAntallBrett >= 0) {
            dybdeCounter[dybde]++;
            //System.out.println("Match detected " + dybde);
            return(cachetAntallBrett);
        }

        return(-1);
    }

    // legger til hash hvis ruta kalte sjekkForHash tidligere.
    public void leggTilHash(int dybde, long antBrett) {
        if (dybde >= maksDybde) return;

        /*if (vedDybde == -1)
            throw new IllegalStateException("Kalte leggTilHash uten sjekkForHash!");*/

        leggTilCache(representasjonerTilLong(representasjonerReferanser), antBrett);
    }
}