import java.io.*;

// Lese- og skrivemetoder for string-brett (Norvig- eller 123456789//123456789-type)

class SudokuSerialisering {
	private static int[] gjettBoksStorrelse(int maksverdi) {
		// Anta at boksstørrelsen er AxB med A,B>1 og A så nærme B som 
		// mulig.

		int rekord = -1;

		for (int i = 2; i <= Math.sqrt(maksverdi); ++i) {
			if (maksverdi % i == 0)
				rekord = i;
		}

		// Hvis nå B = rekord, A = maksverdi/rekord så er A >= B. For en
		// 6er-Sudoku betyr dette at vi får 3x2. For en 9er-Sudoku får 
		// vi 3x3 og for en 16er så blir det 4x4 (ikke 8x2).

		if (rekord == -1)
			throw new IllegalArgumentException("Maksverdi er et " +
				"primtall!");

		int[] returverdi = {maksverdi/rekord, rekord};

		return (returverdi);
	}

	public static Brett lesBrettFraString(String enkelSudokuString,
		int xStorrelse, int yStorrelse) {
		// Tar en string der man leser av verdier fra venstre mot høyre,
		// topp mot bunn i en lang remse av bokstaver. En tom rute er 
		// alt som ikke er en verdi [1-9 i en 9x9-Sudoku].

		int maksverdi = xStorrelse * yStorrelse;
		Brett utBrett = new Brett(xStorrelse, yStorrelse);

		int pos = 0;

		for (int y = 0; y < maksverdi; ++y)
			for (int x = 0; x < maksverdi; ++x)
				try {
					char denneRuta = enkelSudokuString.charAt(pos++);

					// Tving ruta til denne verdien selv hvis det blir en
					// selvmotsigelse.
					utBrett.settUtfyltFraPrintbarVerdi(x, y, denneRuta);
				} catch (java.lang.IllegalArgumentException e) {
					// Bokstav som ikke hører til brettet - bare fortsett.
				}

		return (utBrett);
	}

	public static Brett lesBrettFraString(String innSudokuString) {
		int[] boksLengder = null;

		// For at stringen skal være et gyldig brett, må antall bokstaver være
		// et kvadrat (rad * kolonne). Hvis den ikke er det, hiv en exception.

		int antattMaks = (int)Math.round(Math.sqrt(innSudokuString.length()));
		IllegalArgumentException feilLengde = new IllegalArgumentException(
			"Inputstring har feil lengde!");

		if (antattMaks * antattMaks != innSudokuString.length()) 
			throw feilLengde;

		try {
			boksLengder = gjettBoksStorrelse(antattMaks);
		} catch (IllegalArgumentException e) { throw feilLengde; }

		return(lesBrettFraString(innSudokuString, boksLengder[0], boksLengder[1]));
	}

	public static Brett lesBrettFraUtvidetString(String sudokuString) {
		// Denne stringen har (xlengde, ylengde) foran selve stringen som er
		// som ovenfor (alle gyldige tegn er fylte ruter, resten er tomme).

		// Finn første ). Alt opptil denne er "metadata".
		int fant = -1;
		for (int i = 0; i < sudokuString.length() && fant == -1; ++i)
			if (sudokuString.charAt(i) == ')') fant = i;

		if (fant == -1)
			return lesBrettFraString(sudokuString);

		// Fjern også ( som er den første bokstaven.
		String metadata = sudokuString.substring(1, fant);
		String[] tall = metadata.split(",");

		int boksXLengde = new Integer(tall[0]),
			boksYLengde = new Integer(tall[1]);

		return(lesBrettFraString(sudokuString.substring(fant+1),
			boksXLengde, boksYLengde));
	}

	private static String skrivBrettTilString(Brett brettet, String delimiter) {
		// Gjør det motsatte av LesBrettFraString.

		StringBuffer utString = new StringBuffer();

		for (int y = 0; y < brettet.giMaksverdi(); ++y) {
			for (int x = 0; x < brettet.giMaksverdi(); ++x) {
				utString.append(brettet.giPrintbarVerdi(x, y));
			}
			utString.append(delimiter);
		}

		return(utString.toString());
	}

	public static String skrivBrettTilString(Brett brettet) {
		// Gjør det motsatte av LesBrettFraString.
		return (skrivBrettTilString(brettet, ""));
	}

	// Samme som skrivBrettTilString men med // mellom hver linje
	public static String skrivBrettTilAltString(Brett brettet) {
		return (skrivBrettTilString(brettet, "//"));
	}

	public static String skrivBrettTilUtvidetString(Brett brettet) {
		return ("(" + brettet.giBoksXLengde() + "," + brettet.giBoksYLengde() +
			")" + skrivBrettTilString(brettet));
	}

	public static Brett kopier(Brett original) {
		return lesBrettFraUtvidetString(skrivBrettTilUtvidetString(original));
	}
}

// Formater som ble ønsket i obligen.

class OSudokuSerialisering {

	// Med noen typer exceptions så klager Java høylytt hvis du ikke setter
	// inn throws her. Med andre typer, går det helt greit. Go figure.
	private static Brett lesBrettFraFil(FileReader fr) 
		throws IOException {

		BufferedReader br;
		br = new BufferedReader(fr);

		// Les først to tall (x og y-størrelse, selv om vi egentlig
		// ikke trenger dem).

		int yStorrelse = Integer.parseInt(br.readLine());
		int xStorrelse = Integer.parseInt(br.readLine());

		int maksverdi = xStorrelse * yStorrelse;

		if (xStorrelse < 0 || yStorrelse < 0 || maksverdi < 0 ||
			maksverdi > 36)

			throw new IllegalArgumentException("SudokuLeser: x- og"+
				" y-størrelse gir ikke mening!");

		Brett utBrett = new Brett(xStorrelse, yStorrelse);

        for (int y = 0; y < maksverdi; ++y) {
            String linje = br.readLine().replaceAll("\\r|\\n", "");

			if (linje == null)
				throw new IllegalArgumentException(
					"SudokuLeser: for få linjer!");

			if (linje.length() < maksverdi)
				throw new IllegalArgumentException(
					"SudokuLeser: for kort linje!");

			// TODO: Fuzz testing som tar vekk noen av elementene.
			// Hvis den fuzza versjonen har færre løsninger enn den ufuzza, da
			// er noe galt.
			for (int x = 0; x < maksverdi; ++x)
				try {
					char denneRuta = linje.charAt(x);

					utBrett.settFraPrintbarVerdi(x, y, denneRuta, true);
				} catch (IllegalArgumentException e) {
					// Bokstav som ikke hører til brettet -
					// bare fortsett.
				}
		}

		return(utBrett);
	}

	public static Brett lesBrettFraFil(String 
		filplassering) throws FileNotFoundException, IOException {
		return (lesBrettFraFil(new FileReader(filplassering)));
	}

	// This isn't Python!
	public static Brett lesBrettFraFil(File fila) 
		throws FileNotFoundException, IOException {

		return (lesBrettFraFil(new FileReader(fila)));
	}

	// Går ikke med generics. Thank you, Java!
	public static void skrivBrett(Brett fra, PrintStream til) {
		// Først y og x
		til.println(fra.giBoksYLengde() + "\n" + fra.giBoksXLengde());

		// Så innholdet.
		for (int y = 0; y < fra.giMaksverdi(); ++y) {
			for (int x = 0; x < fra.giMaksverdi(); ++x) {
				til.print(fra.giPrintbarVerdi(x, y));
			}
			til.print("\n");
		}
	}
}
