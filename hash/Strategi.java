import java.util.ArrayList;
import java.util.HashSet;

// Klasse for elimineringsstrategier slik man bruker når man løser Sudoku
// for hånd.

// begrensGruppe tilsvarer "only square"-strategien i Sudoku.
// Siden vi bruker samme brettet på alle rekursjonsnivåer, inneholder klassen
// også en logg over forandringer vi gjorde slik at vi kan reversere dem når
// iterasjonen over en bestemt rute er ferdig.

class Strategi {

	Rad[] rader;
	Boks[] bokser;
	Kolonne[] kolonner;

	int[] antRuterSomForbyrVerdi;

	ArrayList<ArrayList<Rute> > logg;	// ruter vi forandret

	int maksverdi;

	Strategi(Rad[] brettRader, Boks[] brettBokser, Kolonne[] brettKolonner, int maksverdiInn) {
		rader = brettRader;
		bokser = brettBokser;
		kolonner = brettKolonner;
		maksverdi = maksverdiInn;

		antRuterSomForbyrVerdi = new int[maksverdi+1];
		logg = new ArrayList<ArrayList<Rute> >(maksverdi*maksverdi);

		for (int i = 0; i < maksverdi*maksverdi; ++i)
			logg.add(new ArrayList<Rute>());
	}

	// Naked single
	private boolean festRuter(int loggIndex) {
		boolean forandret = false;

		for(SudokuGruppe rad: rader) {
			for (Rute r: rad) {
				if (r.giAntallLovligeVerdier() != 1) continue;

				// Det er mulig å bruke bitfikling her. But don't optimize yet.
				boolean festetLokal = false;
				for (int verdi = 1; verdi <= maksverdi && !festetLokal; ++verdi)
					if (r.erVerdiTillatt(verdi)) {
						r.settVerdi(verdi);
						logg.get(loggIndex).add(r);
						forandret = true;
						festetLokal = true;
					}
			}
		}

		return(forandret);
	}

	private boolean begrensGruppe(SudokuGruppe gruppe, int loggIndex) {
		int i;

		boolean fantnoe = false;
		for (int tall = 1; tall <= maksverdi; ++tall) {
			antRuterSomForbyrVerdi[tall] = 0;

			if (!gruppe.erVerdiTillatt(tall)) 
				continue;

			for (Rute r : gruppe)
				if (!r.erVerdiTillatt(tall))
					++antRuterSomForbyrVerdi[tall];
		}

		ArrayList<Integer> unikeTall = new ArrayList<Integer>();

		for (i = 1; i <= maksverdi; ++i) {
			if (antRuterSomForbyrVerdi[i] == maksverdi-1) {
				unikeTall.add(Integer.valueOf(i));
			}
		}

		for (Rute r: gruppe) {
			int unikeTallIDenneRuta = 0;
			int sisteUnike = -1;

			for (Integer muligUnik: unikeTall) {
				if(r.erVerdiTillatt(muligUnik)) {
					++unikeTallIDenneRuta;
					sisteUnike = muligUnik;
				}
			}

			if (unikeTallIDenneRuta == 1) {
				fantnoe = true;
				r.settVerdi(sisteUnike);
				logg.get(loggIndex).add(r);
			}
		}

		return(fantnoe);
	}

	// Returnerer true om noe skjedde.
	public boolean begrensAlle(int index) {
		//if (logg.get(index).size() > 0)
		//	throw new IllegalStateException("Prøvde å kalle begrens uten å frigjøre først.");

		boolean fantnoe = true;
		boolean fantnoeEnGang = false;
		while (fantnoe) {
			fantnoe = false;
			
			for (Rad r: rader)
				fantnoe |= begrensGruppe(r, index);

			for (Kolonne k: kolonner)
				fantnoe |= begrensGruppe(k, index);

			for (Boks b: bokser)
				fantnoe |= begrensGruppe(b, index);

			fantnoeEnGang |= fantnoe;
		}

		return(fantnoeEnGang);
	}

	public void frigiAlle(int index) {
		for (Rute r: logg.get(index)) {
			r.fjernVerdi();
		}

		logg.get(index).clear();
	}

	// Se Rute.java
	final int BOKS = 0, RAD = 1, KOLONNE = 2;

	// QND
	private HashSet<SudokuGruppe> giTilhorendeGrupper(SudokuGruppe boks, int gruppeType) {
		HashSet<SudokuGruppe> grupper = new HashSet<SudokuGruppe>();

		for (Rute rute: boks)
			grupper.add(rute.giTilhorendeSudokuGruppe(gruppeType));

		return(grupper);
	}

	// Finner den første verdien som er i a men ikke i b. Gir exception
	// hvis en slik ikke finnes. (Siden det indikerer en bug så tar vi
	// ikke imot exception i funksjoner som bruker denne.)
	private int finnVerdiIForste(HashSet<Integer> a, HashSet<Integer> b) {
		for (int i: a)
			if (!b.contains(i))
				return(i);

		throw new IllegalStateException("Fant ikke verdi i a");
	}

	// Finner ruta som fins både i a og b. Gir exception hvis den ikke
	// finnes.
	private Rute finnKryssendeRute(SudokuGruppe a, SudokuGruppe b) {
		for (Rute r: a)
			if (b.erRuteLagtTil(r))
				return(r);

		throw new IllegalStateException("Finnes ingen kryssende rute!");
	}

	// De to parametrene "paabudteRadVerdier" og "paabudtRad" blir forandret. 
	// Den første blir fylt med verdiene som må være i denne raden (anta gruppa
	// er en rad). Den andre blir fylt med radene i denne boksen. Returnerer true
	// hvis alt gikk greit, false hvis den oppdaget en selvmotsigelse (flere 
	// påbudte verdier i en rad enn det er ruter i raden).
	public boolean finnPaabudteVerdier(SudokuGruppe boks,
		ArrayList<HashSet<Integer> > paabudteRadVerdier,
		ArrayList<SudokuGruppe> paabudtRad, 
		HashSet<SudokuGruppe> raderIBoks) {

		int antRuterIRad = maksverdi/raderIBoks.size();
		
		for (SudokuGruppe r: raderIBoks) {
			HashSet<Integer> paabudtDenneRad = null;

			for (SudokuGruppe annen: raderIBoks) {
				if (annen == r) continue;

				if (paabudtDenneRad == null)
					paabudtDenneRad = annen.giBrukteVerdier();
				else 
					paabudtDenneRad.retainAll(annen.giBrukteVerdier());
			}

			if (paabudtDenneRad == null) continue;

			// Fjern verdiene som allerede er satt, siden det ville vært
			// rart om vi sa at 1 var påbudt i denne raden hvis 1 er satt
			// et annet sted...
			paabudtDenneRad.removeAll(boks.giBrukteVerdier());

			//System.out.println("Påbudt i rad: " + paabudtDenneRad);
			//System.out.println("Forbudt i rad: " + r.giBrukteVerdier());

			if (paabudtDenneRad.size() > antRuterIRad) {
				//System.out.println("Contradiction detected.");
				return(false);
			}

			paabudteRadVerdier.add(paabudtDenneRad);
			paabudtRad.add(r);
		}

		return(true);
	}

	// Jeg vet ikke hva denne strategien heter, så derfor det uinformative
	// navnet "testing". Kanskje oppdagSelvmotsigelse? Men den kan også 
	// "spikre fast" verdier i visse tilfeller.

	// Strategien ble originalt konstruert mtp bokser, rader og kolonner.
	// Derfor går kommentarene utifra dette selv om strategien også kan
	// brukes om andre kombinasjoner (men da er veldig vanskelig å visualisere).
	private int testing(SudokuGruppe boks, int gruppeTypeA, int gruppeTypeB,
		int loggIndex) {
		// Hvis boksen er full er det ikke noe mer å gjøre.
		if (boks.giBrukteVerdier().size() == maksverdi)
			return(0);
		// 1. Finn forbudte og påbudte verdier for hver rad i boksen.
		//		(Forbudte verdier er verdier som ikke kan være i den
		//		raden. Tvungne/forbudte verdier er verdier som må være et 
		//		eller annet sted i den raden for at boksen skal kunne fylles 
		//		ut.)

		// 2. Hvis det fins et snitt av forbudte verdier i en rad og
		//		påbudte verdier i en kolonne slik at snittet har minst
		//		like mange elementer som det er ruter i kolonnen (i boksen),
		//		så kan ikke oppgaven løses.

		// 3. Samme med forbudte verdier i en kolonne og tvungne verdier
		//		i en rad.

		// Optimisere senere (hvis jeg har tid). Den finner ikke alle 
		// selvmotsigelser som strategien i seg selv ville funnet, siden den
		// ikke tar høyde for verdier som allerede er satt (f.eks hvis snitt av
		// forbudte verdier i en rad og påbudte i en kolonne er 3 men 1 verdi
		// er allerede satt i den kolonnen, da har man et problem, men det
		// finner ikke denne ut av).
		//		Kan også gjøre triks med at hvis snittet har lengde
		//		(# i kolonne - 1) så vet vi hva det siste elementet blir...

		HashSet<SudokuGruppe> raderIBoks = giTilhorendeGrupper(boks, gruppeTypeA);
		int antRuterIRad = maksverdi/raderIBoks.size();

		// Finn verdier som må være i en bestemt rad i boksen. Disse
		// er verdiene som ikke kan være i noen annen rad.

		ArrayList<HashSet<Integer> > paabudteRadVerdier = 
			new ArrayList<HashSet<Integer> >();
		// Raden det er snakk om
		ArrayList<SudokuGruppe> paabudtRad = new ArrayList<SudokuGruppe>();

		if (!finnPaabudteVerdier(boks, paabudteRadVerdier, paabudtRad, 
			raderIBoks)) return(-1);

		// Så gjør vi det samme med kolonner.

		HashSet<SudokuGruppe> kolonnerIBoks = giTilhorendeGrupper(boks, gruppeTypeB);
		int antRuterIKolonne = maksverdi/kolonnerIBoks.size();

		ArrayList<HashSet<Integer> > paabudteKolonneVerdier = 
			new ArrayList<HashSet<Integer> >();
		// Kolonnen det er snakk om
		ArrayList<SudokuGruppe> paabudtKolonne = new ArrayList<SudokuGruppe>();

		if (!finnPaabudteVerdier(boks, paabudteKolonneVerdier, paabudtKolonne, 
			kolonnerIBoks)) return(-1);

		// Hvis det nå finnes en rad og en kolonne slik at snittet av de
		// påbudte i raden og de forbudte i kolonnen er like lang som
		// antall ruter i raden, har vi en selvmotsigelse. Dette fordi
		// vi vet at en av radens ruter må krysse kolonnen. Men siden
		// alle mulige ruter i raden er forbudt i kolonnen, kan ikke den
		// kryssende ruta ha noen som helst lovlig verdi.

		int radIndex = 0;

		for (HashSet<Integer> paabudtIRad : paabudteRadVerdier) {
			SudokuGruppe denneRad = paabudtRad.get(radIndex++);

			for (SudokuGruppe k: kolonnerIBoks) {
				HashSet<Integer> kopi = new HashSet<Integer>(paabudtIRad);
				kopi.retainAll(k.giBrukteVerdier());

				if (kopi.size() >= antRuterIRad)
					return(-1);

				if (kopi.size() == antRuterIRad-1 && paabudtIRad.size() == antRuterIRad) {
					// Nå må den kryssende ruta inneholde den eneste verdien som ikke er i kopi
					// men som er i paabudtIRad.
					int tvungenVerdi = finnVerdiIForste(paabudtIRad, kopi);
					Rute kryssendeRute = finnKryssendeRute(denneRad, k);
					if (kryssendeRute.harVerdi() && kryssendeRute.giVerdi() != tvungenVerdi)
						return(-1);
					if (!kryssendeRute.harVerdi()) {
						kryssendeRute.settVerdi(tvungenVerdi);
						logg.get(loggIndex).add(kryssendeRute);
						return(1);
					}
				}
			}
		}

		int kolonneIndex = 0;

		for (HashSet<Integer> paabudtIKolonne : paabudteKolonneVerdier) {
			SudokuGruppe denneKolonne = paabudtKolonne.get(kolonneIndex++);
			for (SudokuGruppe r: raderIBoks) {
				HashSet<Integer> kopi = new HashSet<Integer>(paabudtIKolonne);
				kopi.retainAll(r.giBrukteVerdier());

				if (kopi.size() >= antRuterIKolonne)
					return(-1);

				if (kopi.size() == antRuterIKolonne-1 && paabudtIKolonne.size() == antRuterIKolonne) {
					// Nå må den kryssende ruta inneholde den eneste verdien som ikke er i kopi
					// men som er i paabudtIKolonne.
					int tvungenVerdi = finnVerdiIForste(paabudtIKolonne, kopi);
					Rute kryssendeRute = finnKryssendeRute(denneKolonne, r);
					if (kryssendeRute.harVerdi() && kryssendeRute.giVerdi() != tvungenVerdi)
						return(-1);
					if (!kryssendeRute.harVerdi()) {
						kryssendeRute.settVerdi(tvungenVerdi);
						logg.get(loggIndex).add(kryssendeRute);
						return(1);
					}
				}
			}
		}

		return(0);
	}

	private boolean massTesting(int logIndex, SudokuGruppe[] grupper, 
		int gruppeTypeA, int gruppeTypeB)  {

		for (SudokuGruppe gruppe: grupper) {
			switch(testing(gruppe, gruppeTypeA, gruppeTypeB, logIndex)) {
				case -1: throw new RuntimeException(); // selvmotsigelse
				case 0: break;
				case 1: return(true);	// alt ok, gjorde noen forandringer
			}
		}

		return(false); // alt ok, ingen forandringer
	}

	public boolean massTesting(int index, Brett brettet) {
		boolean gjordeNoe = true;
		while (gjordeNoe) {
			if (festRuter(index)) continue;
			if (begrensAlle(index)) continue;

			try {
				gjordeNoe = massTesting(index, bokser, RAD, KOLONNE);
				// Av en eller annen grunn fungerer ikke disse, og jeg
				// har ikke tid til å gjøre noe med det, så jeg har
				// bare kommentert dem ut. TODO: FIX.

				//gjordeNoe |= massTesting(index, rader, BOKS, KOLONNE);
				//gjordeNoe |= massTesting(index, kolonner, BOKS, RAD);
				if (gjordeNoe) continue;
			} catch (RuntimeException e) {
				return(false);
			}
		}
		
		
		return(true);
	}

	public boolean optimalisere(int dybde, Brett brettet) {
		return(massTesting(dybde, brettet));
	}
	
}

// Hidden Twin? Hvis du har to ruter i samme rad/boks/kolonne og intersect
// av de mengdene de kan ha er av størrelse 2, så kan ikke disse ha noen
// andre enn de to.