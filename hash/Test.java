import java.io.*;
import java.util.Random;
import java.util.ArrayList;

class Kjoretest {

	public Brett tilfeldigBrett() {
		int maksverdi;
		Random rng = new Random();

		switch(rng.nextInt(3)) {
			case 0: return(tilfeldigBrett(3, 2));
			default:
			case 1: return (tilfeldigBrett(3, 3));
			case 2: return (tilfeldigBrett(4, 4));
		}
	}

	public Brett tilfeldigBrett(int xStorrelse, int yStorrelse) {

		Random rng = new Random();

		Brett brettet = new Brett(xStorrelse, yStorrelse);

		int maksverdi = xStorrelse * yStorrelse;

		// Legg til (maksverdi*2)-1 forskjellige tall i det tilfeldige brettet.
		// Det tilsvarer 17 for 9x9.
		//int antallTall = (maksverdi*2)-1 + rng.nextInt(maksverdi*maksverdi - (maksverdi * 2)-1);
		int antallTall = (maksverdi*5) + rng.nextInt(maksverdi*maksverdi - maksverdi*5);

		for (int i = 0; i < antallTall; ++i) {
			boolean harSatt = false;

			for (int j = 0; j < 100 && !harSatt; ++j) {
				int randX = rng.nextInt(maksverdi),
					randY = rng.nextInt(maksverdi);

				int randTall = rng.nextInt(maksverdi) + 1;

				harSatt |= brettet.settVerdi(randX, randY, randTall);
			}

			// Hvis det ikke gikk, start fra børjan.
			if (!harSatt)
				return (tilfeldigBrett());
		}

		return(brettet);
	}

	public void testTom() {
		System.out.println("750 empty boards, coming right up...");

		Brett tomt = new Brett(3, 3);

		tomt.printBrett();

		SudokuBeholder losninger = new SudokuBeholder(750);

		tomt.fyllUt(losninger);

		for (Brett b: losninger)
			b.printBrett();

	}

	public void testFraFil(String stiFilnavn, String filnavn, boolean visTiming,
		boolean alleKanLoses) throws IOException {

		BufferedReader br = new BufferedReader(new FileReader(stiFilnavn));
		int i = 0;

		try {
			String linje = br.readLine();

			while (linje != null) {
				if (linje.length() > 0) {
					long startTid = System.nanoTime();
					SudokuBeholder losninger = new SudokuBeholder(750);
					SudokuSerialisering.lesBrettFraString(linje).fyllUt(losninger);
					if (losninger.hentAntallLosninger() > 0)
						System.out.print("!");
					else {
						System.out.print(".");
						if (alleKanLoses) {
							System.out.println("\n" + linje);
							throw new IllegalStateException("Fant uløselig Sudoku!");
						}
					}
					if (visTiming) {
						System.out.println(" " + linje + " " + (System.nanoTime() - startTid)
							+ " " + filnavn);
					}
				}
				linje = br.readLine();
			}
		} catch (IOException e) {
		} finally {
			br.close();
			System.out.println("\n");
		}
	}

	public void testFraObligFiler(String[] filer, boolean visTiming, 
		boolean alleKanLoses) throws IOException {

		for (String filnavn: filer) {
			long startTid = System.nanoTime();
			SudokuBeholder losninger = new SudokuBeholder(750);
			Brett detteBrettet = OSudokuSerialisering.lesBrettFraFil(filnavn);
			detteBrettet.printBrett();
			detteBrettet.fyllUt(losninger);
			if (losninger.hentAntallLosninger() > 0) {
				System.out.print("!");
				System.out.println(losninger.hentAntallLosninger());
				losninger.taUt(0).printBrett();
			} else {
				System.out.print(".");
				if (alleKanLoses) {
					System.out.println("\n" + filnavn);
					throw new IllegalStateException("Fant uløselig Sudoku!");
				}
			}

			if (visTiming) {
				System.out.println(" " + filnavn + " " + (System.nanoTime() - 
					startTid));
			}
		}
	}

	// Every good regulator of a system is a model of that system.
	public void benchmarkRandom(int hvorMange) {
		ArrayList<Brett> testBrett = new ArrayList<Brett>(hvorMange);

		System.out.println("Genererer brett...");
		int i;

		for (i = 0; i < hvorMange; ++i) {
			testBrett.add(tilfeldigBrett());
			System.out.print(".");
		}

		System.out.println("Ferdig.");

		long startTid = System.nanoTime();

		System.out.println("Benchmarking time:");

		for (i = 0; i < hvorMange; ++i) {
			SudokuBeholder losninger = new SudokuBeholder(750);
			System.out.print("(" + testBrett.get(i).giMaksverdi() + ")");
			testBrett.get(i).fyllUt(losninger);
			if (losninger.hentAntallLosninger() > 0)
				System.out.print("!");
			else
				System.out.print(".");
			System.out.flush();
		}

		long stoppTid = System.nanoTime();

		System.out.format("%nDet tok %.2f sekunder.%n", (stoppTid-startTid)/1e9);
	}

	public void printTilfeldigeLosbare(int hvorMange, int xStorrelse,
		int yStorrelse, boolean bareEnLosning) {

		for (int i = 0; i < hvorMange; ++i) {
			Brett brettet = tilfeldigBrett(xStorrelse, yStorrelse);
			SudokuBeholder losninger;
			if (bareEnLosning) 
				losninger = new SudokuBeholder(2);
			else
				losninger = new SudokuBeholder(1);

			String brettPrintable = SudokuSerialisering.skrivBrettTilString(brettet);
			brettet.fyllUt(losninger);
			if (losninger.hentAntallLosninger() == 1)
				System.out.println("\n" + brettPrintable);
			else {
				if (losninger.hentAntallLosninger() > 1)
					System.out.print("#");
				else
					System.out.print(".");
			}
		}
	}

	public void benchmark() {

		long startTid = System.nanoTime();

		boolean visDetaljertTiming = false;

		String benchmarks[] = { "easy50", "minimal", "slow", "1000_9x9", 
			"sudoku17subset", "solvables.txt", "mixed", "top95", "hardest.txt", "6ere", 
			"9ere", "16ere", "16x16_internet", "16x16random" };

		try {
			for (String benchfile : benchmarks) {
				System.out.println(benchfile);
				testFraFil("benchmark/" + benchfile, benchfile, visDetaljertTiming, 
					true);
			}			
		} catch (IOException e) {
			System.out.println("En av filene kunne ikke leses.");
		}

		long stoppTid = System.nanoTime();

		System.out.format("%nDet tok %.2f sekunder.%n", (stoppTid-startTid)/1e9);
	}

	public void testObligFiler() {
		String p = "input/";

		/*String obligFiler[] = { p + "6x6oppg28losn.txt", p + "9x9_1.txt",
			p + "6x6_tom.txt", p + "variants/36x36.txt",
			p + "9x9_2.txt", p + "brett.12.1.txt", p + "brett.12.2.txt",
			p + "brett.16.1.txt", p + "brett.6.1.txt", p + "brett.6.2.txt",
			p + "brett.9.1.txt", p + "brett.9.3.txt", p + "brett.9.5.txt",
			p + "brett.9.6.txt", p + "brett.9.7.txt", p + "brett.9.8.txt",
			p + "brett.9.9.txt", p + "sudokuoppgave6x6.txt",
			p + "sudokuoppgave9x9.txt" };*/

		String obligFiler[] = { p + "36x36bond.txt"};

		try {
			long startTid = System.nanoTime();

			testFraObligFiler(obligFiler, true, false);

			long stoppTid = System.nanoTime();
			System.out.format("%nDet tok %.2f sekunder.%n", (stoppTid-startTid)/1e9);

		} catch (IOException e) {
			System.out.println("Fikk noe problemer med å lese fra filene");
		}
	}
	
	public void test() {
		System.out.println("Doing something funny");

		String empty =      ".................................................................................";
		String impossible = ".....5.8....6.1.43..........1.5........1.6...3.......553.....61........4.........";
		String easy = "003020600900305001001806400008102900700000008006708200002609500800203009005010300";
		String easyTest = "xx0000000009805100051907420290401065000000000140508093026709580005103600000000000";
		String weeklyHard = "..9..3.6.....4.1..5..1......9.....2.8.....4...27..6..9...........3..2.7....85.6..";
		String weeklyHardII = "5..9..........1..9.8..6..2..3....2.8..8.3....7.6....3....4..1...7..8..6...4..5..2";
		String iv = ".....6....59.....82....8....45........3........6..3.54...325..6..................";
		String qs = "...8.1..........435............7.8........1...2..3....6......75..34........2..6..";
		String otherhard = ".52..68.......7.2.......6....48..9..2..41......1.....8..61..38.....9...63..6..1.9";
		String setwiseKlarerIkke = "13...54.......8..........2...6.3....2.37.......8.......8..5......4.....3......5..";
		String supposed = "..2...6.7...2.8....3......9..8.2............8......1..3....6...6....3........2.7.";
		String solvable = "14.2.............6..........61..........59....8..6.....53.........7...6...23....1";
		String elimtest = "100070380000300109050000000809000005007000060000160000006800000700002400002030000";

		// Denne skal tvinge alle cellene til å være 1 selv om det er en selvmotsigelse.
		String contradict="111111111111111111111111111111111111111111111111111111111111111111111111111111111";

		Brett brettet = SudokuSerialisering.lesBrettFraString(contradict);//new Brett(9);

		brettet.printBrett();

		SudokuBeholder losninger = new SudokuBeholder(2);

		brettet.fyllUt(losninger);

		System.out.println("Antall løsninger: " + losninger.hentAntallLosninger());

		for (Brett b: losninger) {
			b.printBrett();
		}

	}
}

/*class TestRun {
	public static void main(String[] args) {
		//new Kjoretest().printTilfeldigeLosbare(10000, 4, 4, true);
		//new Kjoretest().test();
		//new Kjoretest().benchmark();
		new Kjoretest().testObligFiler();
		//new Kjoretest().benchmarkRandom(5000);
	}
}*/
