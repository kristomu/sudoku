// Zobrist-lignende hash for SudokuGrupper.

// Denne klassen blir brukt for å lage en hash slik at vi kan lagre halvløste
// Sudokuer som vi vet kan løses eller ikke. Hvis vi da senere kommer borti en
// halvløst Sudoku som har samme begrensninger på tallene som en tidligere, så
// vet vi at den andre kan løses bare hvis den første kan det.

// Hver SudokuGruppe har en Zobrist-klasse og disse refererer alle til en felles
// Integer som forandres når noe forandres i hver rute; men hver rute forandrer 
// den på forskjellig vis når f.eks. tall nr 9 blir satt inn eller fjernet.

import java.util.Random;

// ??? Er dette pen kode? Det er iallfall lettere enn å fikle med Integer.
// TODO: Refactor dette slik at det gjøres i HashStrategi isteden.
class ZobristIntBeholder {
	public int hash;
}

class Zobrist {
	private ZobristIntBeholder hashContainer;
	private int hasher[];

	Zobrist(int maksverdi, ZobristIntBeholder fellestall) {
		hasher = new int[maksverdi+1];

		Random generator = new Random();

		for (int i = 0; i <= maksverdi; ++i) {
			// Vi har ikke lyst på negative tall: de gjør det vanskeligere å hashe.
			do {
				hasher[i] = generator.nextInt();
			} while (hasher[i] < 0);
		}

		hashContainer = fellestall;
	}

	// Skrur en verdi "av" eller "på".
	// NB: Ingen feilsjekking siden dette må være /raskt/.
	private void flippHash(int verdi) {
		hashContainer.hash ^= hasher[verdi];
	}

	public void settVerdi(int verdi) {
		flippHash(verdi);
	}

	public void frigiVerdi(int verdi) {
		flippHash(verdi);
	}

	public int giHash() {
		return(hashContainer.hash);
	}
}