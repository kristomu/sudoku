import java.util.*;

class Brett {
	Rute[][] ruter;			// [y][x] (row-major order)

	Rad[] rader;
	Boks[] bokser;
	Kolonne[] kolonner;

	Strategi eliminering;
	DeaktiveringsListe<Rute> nesteRuter;

	int[] dybdeCounter;

    // Kan brukes til å sette en grense på hvor mange løsninger vi er
    // interessert i å telle, f.eks. ved fuzzing (trenger bare å vite at
    // vi har flere løsninger enn det vi hadde før fuzzingen) eller ved
    // søk etter nye brett.
    long maksOnskedeLosninger;

	private SudokuGruppe finnTilhorendeGruppe(SudokuGruppe[] grupper, int x, int y) {
		
		for (SudokuGruppe g: grupper)
			if (g != null && g.tilhorerRuteDenne(x, y))
				return(g);

		return(null);
	}

	public ArrayList<Long> giRepresentasjon() {
        ArrayList<Long> representasjon = new ArrayList<Long>();

        for (Rad r: rader) {
                representasjon.add(r.giRepresentasjon());
        }

        for (Kolonne k: kolonner) {
                representasjon.add(k.giRepresentasjon());
        }

        for (Boks b: bokser) {
                representasjon.add(b.giRepresentasjon());
        }

        return(representasjon);
    }

	// Her er maksverdi den største verdien en rute kan ha.
	Brett(int boksXLengde, int boksYLengde) {

        /////////////////////////////////////////////

		int maksverdi = boksXLengde * boksYLengde;

		dybdeCounter = new int[maksverdi*maksverdi];

		// Sett opp rader, kolonner og bokser
		rader = new Rad[maksverdi];
		bokser = new Boks[maksverdi];
		kolonner = new Kolonne[maksverdi];

		for (int i = 0; i < maksverdi; ++i) {
			rader[i] = new Rad(i, maksverdi);
			bokser[i] = new Boks(i, boksXLengde, boksYLengde);
			kolonner[i] = new Kolonne(i, maksverdi);
		}

		// Sett opp ruter
		ruter = new Rute[maksverdi][maksverdi];

		for (int y = 0; y < maksverdi; ++y)
			for (int x = 0; x < maksverdi; ++x) {
				ruter[y][x] = new TomRute(maksverdi,
					finnTilhorendeGruppe(bokser, x, y), finnTilhorendeGruppe(rader, x, y),
					finnTilhorendeGruppe(kolonner, x, y),
					y * giMaksverdi() + x);

				finnTilhorendeGruppe(rader, x, y).leggTilRute(ruter[y][x]);
				finnTilhorendeGruppe(bokser, x, y).leggTilRute(ruter[y][x]);
				finnTilhorendeGruppe(kolonner, x, y).leggTilRute(ruter[y][x]);
			}
			
		eliminering = new Strategi(rader, bokser, kolonner, maksverdi);

		maksOnskedeLosninger = -1;
	}

	private void settRuteRekkefolge(boolean shuffle) {
		if (giMaksverdi() <= 0)
			return;

		class RuteVedLovlige implements Comparable<RuteVedLovlige> {
			public Rute r;

			RuteVedLovlige(Rute rInn) {
				r = rInn;
			}

			public int compareTo(RuteVedLovlige other) {
				if (other == null) return(1);
				if (this == null) return(-1);
				
				return ((int)(other.r.giAntallLovligeVerdier() - 
					r.giAntallLovligeVerdier()));
			}
		}

		ArrayList<RuteVedLovlige> ytelse = new ArrayList<RuteVedLovlige>();

		for (Rute[] rk : ruter) {
			for (Rute r: rk) {
				ytelse.add(new RuteVedLovlige(r));
			}
		}

		if (shuffle)
			Collections.shuffle(ytelse);
		else
			Collections.sort(ytelse, Collections.reverseOrder());

		nesteRuter = new DeaktiveringsListe<Rute>();

		for (RuteVedLovlige rv: ytelse)
			nesteRuter.leggTilSist(rv.r);
	}

	public int giMaksverdi() { return(ruter.length); }

	public int giBoksXLengde() { return(bokser[0].giXLengde()); }
	public int giBoksYLengde() { return(bokser[0].giYLengde()); }

	private boolean eksistererRute(int x, int y) {
		return (x >= 0 && x < giMaksverdi() && y >= 0 && y < giMaksverdi());
	}

	// Gir false hvis det ikke er tillatt.
	public boolean settVerdi(int x, int y, int verdi) {
		return(ruter[y][x].settVerdi(verdi));
	}

	public void settMaksOnskedeLosninger(long molinn) {
		maksOnskedeLosninger = molinn;
	}

	public boolean erFerdigUtfylt(int x, int y) {
		return(ruter[y][x] instanceof UtfyltRute);
	}

	public void settUtfyltFraPrintbarVerdi(int x, int y, char printbarVerdi) {
		if (!Rute.erPrintbarVerdi(printbarVerdi)) return;
		if (ruter[y][x] instanceof UtfyltRute) return;

		UtfyltRute nyRute = new UtfyltRute(giMaksverdi(),
					finnTilhorendeGruppe(bokser, x, y), 
					finnTilhorendeGruppe(rader, x, y),
					finnTilhorendeGruppe(kolonner, x, y), printbarVerdi,
					y * giMaksverdi() + x);

		Rute gammelRute = ruter[y][x];

		finnTilhorendeGruppe(rader, x, y).erstattRute(gammelRute, nyRute);
		finnTilhorendeGruppe(bokser, x, y).erstattRute(gammelRute, nyRute);
		finnTilhorendeGruppe(kolonner, x, y).erstattRute(gammelRute, nyRute);

		ruter[y][x] = nyRute;
	}

	public boolean settFraPrintbarVerdi(int x, int y, char printbarVerdi,
		boolean tving) {
		if (y < 0 || y >= ruter.length) return(false);
		if (x < 0 || x >= ruter[0].length) return(false);

		return(ruter[y][x].settFraPrintbarVerdi(printbarVerdi, tving));
	}

	public void fjernVerdi(int x, int y){
		ruter[y][x].fjernVerdi();
	}

	public int giVerdi(int x, int y) {
		// -1 hvis den er tom.
		return(ruter[y][x].giVerdi());
	}

	public char giPrintbarVerdi(int x, int y) {
		return (ruter[y][x].giPrintbarVerdi());
	}

	public boolean harVerdi(int x, int y) {
		if (!eksistererRute(x, y))
			throw new IllegalArgumentException("out of bounds");

		return (ruter[y][x].harVerdi());
	}

	public int giAntallMedVerdi() {
		int antallMedVerdi = 0;

		for (int x = 0; x < giMaksverdi(); ++x)
			for (int y = 0; y < giMaksverdi(); ++y)
				if (harVerdi(x, y))
					++antallMedVerdi;

		return(antallMedVerdi);
	}

	public boolean finsFeil() {
		for (SudokuGruppe g: rader)
			if (g.finsFeil())
				return(true);

		for (SudokuGruppe g: kolonner)
			if (g.finsFeil())
				return(true);

		for (SudokuGruppe g: bokser)
			if (g.finsFeil())
				return(true);

		return(false);
	}

	public long gaaTilNeste(int dybde, int maksverdi, Brett brettet, 
		SudokuBeholder losninger) {

		// Hvis vi har maks antall løsninger, avslutt.
		if (maksOnskedeLosninger > 0 && 
			losninger.hentAntallTotaleLosninger() >= maksOnskedeLosninger) {
			return(0);
		}

		// Hvis vi ikke kan gå til neste rute, da er vi på siste. 
		// Skriv ut brettet.

		Node<Rute> neste = finnNeste(dybde);
		if (neste == null) {
			losninger.settInn(brettet);
			return(1);
		} else {
			nesteRuter.fjern(neste);
			long brettHerunder = neste.innhold.fyllUtRestenAvBrettet(dybde+1, 
				maksverdi, brettet, losninger);
			nesteRuter.leggTilbakeEnFram(neste);

			return(brettHerunder);
		}
	}

	// Gir neste rute. dybde er 0 for første rute, 1 for neste osv.
	// Dette fungerer ved å finne den ruta som har færrest mulige verdier man må 
	// prøve seg gjennom.
	public Node<Rute> finnNeste(int dybde) {
		// Hvis vi nå er utenfor, returner null.
		if (nesteRuter.len() == 0)
			return(null);

		long record = giMaksverdi() + 1;
		Node<Rute> nruteNode = nesteRuter.forste();
		Node<Rute> recordholder = null;

		//if (nesteRuter.len() < 10) return(nruteNode);

		while (nruteNode != null && record > 1) {
			long lovligeVerdier = nruteNode.innhold.giAntallLovligeVerdier();

			if (lovligeVerdier < record) {
				record = lovligeVerdier;
				recordholder = nruteNode;
			}

			nruteNode = nruteNode.neste;
		}

		return(recordholder);
	}

	/*public void debug() {
		for (int i = 0; i < dybdeCounter.length; ++i) {
			System.out.println("dybde " + i + " hit " + dybdeCounter[i]);
		}
	}*/

	// Start rekursjonen her. Benytter også en del triks for å gjøre det raskere,
	// og for å oppdage selvmotsigelser. Shuffle setter tilfeldig rekkefølge til
	// hvordan vi besøker rutene.
	public long fyllUt(SudokuBeholder losninger, boolean shuffle) {
		// Hvis maksverdien er 0 så trenger vi ikke gjøre noe :p
		if (giMaksverdi() <= 0)
			return(0);

		// Hvis en rute har en ulovlig verdi et sted, trenger vi heller ikke
		// gjøre noe.
		if (finsFeil())
			return(0);

		// Hvis optimaliseringen sier det er en selvmotsigelse nå, trenger
		// vi heller ikke gjøre noe.
		if (!optimalisere(0)) {
			return(0);
		}

		settRuteRekkefolge(shuffle);
		return(gaaTilNeste(0, giMaksverdi(), this, losninger));
	}

	public long fyllUt(SudokuBeholder losninger) {
		return (fyllUt(losninger, false));
	}

	public boolean optimalisere(int dybde) {
		return (eliminering.optimalisere(dybde, this));
	}

	public void avoptimalisere(int dybde) {
		eliminering.frigiAlle(dybde);
	}

	// Skal dette flyttes inn i et view?
	// Bluesky: legg også inn printing av tillatte verdier.
	public void printBrett() {
		for (int y = 0; y < giMaksverdi(); ++y) {
			for (int x = 0; x < giMaksverdi(); ++x) {
					System.out.print(" " + giPrintbarVerdi(x, y));

				if (x % giBoksXLengde() == giBoksXLengde()-1) 
					System.out.print (" |");
			}

			System.out.println("");
			if (y % giBoksYLengde() == giBoksYLengde()-1) {
				for (int x = 0; x < giMaksverdi(); ++x) {
					System.out.print("--");
					if (x % giBoksXLengde() == giBoksXLengde()-1) 
						System.out.print("-+");
				}
				System.out.println("");
			}
		}
		System.out.println("\n");
	}
}
