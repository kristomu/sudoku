import java.io.*;

class Run {
	/* Størrelsen brettet og selve oppgaven (de sifrene som alt er fylt inn) 
	   skal leses fra fil. Filnavnet oppgis som parameter til programmet (på 
	   kommandolinja). Filformatet skal være slik som beskrevet under (gruppe-
	   læren som skal godkjenne oppgaven din vil teste programmet ditt med 
	   andre filer). Hvis det oppgis ett filnavn skal løsningen(e) skrives til 
	   skjerm. Hvis det oppgis to filnavn skal oppgaven løses fra den første 
	   filen, og løsningen(e) skrives på denandre filen (og ikke skrives til 
	   skjerm). Filformatene er beskrevet nedenfor. */

	public static void losTilPrintStream(String filnavnInn, PrintStream ut) {
		Brett detteBrettet = null;
		try {
			detteBrettet = OSudokuSerialisering.lesBrettFraFil(filnavnInn);
		} catch (java.io.FileNotFoundException e) {
			System.err.println("Fant ikke fila " + filnavnInn);
			return;
		} catch (java.io.IOException e) {
			System.err.println("Fikk problemer med å lese fra " + filnavnInn);
			return;
		} catch (NumberFormatException e) {
			System.err.println("Feil format på fil " + filnavnInn);
			return;
		}
		SudokuBeholder losninger = new SudokuBeholder(750);
		ut.println("Starter løsing");

		long startTid = System.nanoTime();
		detteBrettet.fyllUt(losninger);
		long stoppTid = System.nanoTime();

		ut.println("Antall løsninger: " + losninger.hentAntallTotaleLosninger());
		ut.println("Antall lagrede løsninger: " + losninger.hentAntallLosninger());
		ut.format("%nDet tok %.2f sekunder.%n", (stoppTid-startTid)/1e9);

		if (losninger.hentAntallLosninger() < 5) {
			for (Brett b: losninger) {
				ut.println("\n");
				OSudokuSerialisering.skrivBrett(b, ut);
			}
		} else {
			int count = 0;

			for (Brett b: losninger) {
				ut.format("%d: %s\n", ++count, 
					SudokuSerialisering.skrivBrettTilAltString(b));
			}
		}
	}

	public static void main(String[] args) {

		switch(args.length) {
			default:
			case 0:
				// It's GUI time!
				new Sammenkobling().main();
				break;
			case 1:
				// Så må man bare fikse problemet med /SKAL/ HA NEDARVING
				// så er alt ferdig.

				// Last inn brettet som ble gitt som parameter og sleng ut
				// resultatene til skjermen
				losTilPrintStream(args[0], System.out);

				break;
			case 2:
				PrintStream utFilStream;
				try {
					utFilStream = new PrintStream(new File(args[1]));
				} catch (IOException e) {
					System.err.println("Problemer med å skrive til " + args[1]);
					return;
				}
				losTilPrintStream(args[0], utFilStream);
				break;
		}
	}

}