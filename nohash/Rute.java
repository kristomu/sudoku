// TODO? Begrens setter minVerdi og maksVerdi slik at vi ikke trenger å spørre
// så mange ganger. F.eks hvis begrens fastsetter siste til å være enten 8
// eller 9, så sett minverdi til 8 slik at den ikke går gjennom rad/boks/kolonne
// 7 ganger hver gang den rekurserer ned dit.

import java.util.ArrayList;

abstract class Rute {

	protected int verdi, maksVerdi;
	private int id;

	// I C++: typedef enum GruppeRef { BOKS = 0, RAD = 1, KOLONNE = 2};
	// men Javas enum er jo bare en klasse, so a'duplicating we go...
	final int BOKS = 0, RAD = 1, KOLONNE = 2;

	SudokuGruppe tilhorendeSudokuGruppe[];

	// Eventuelle andre verdier som heller ikke er tillatte. Dette brukes av
	// Strategi til å kutte ned på muligheter.
	// Implementere dette når vi fikser Strategi og.
	// private boolean[] ogsaaForbudt;

	Rute(int maksVerdiInn, SudokuGruppe tilhorendeBoks, SudokuGruppe tilhorendeRad, 
		SudokuGruppe tilhorendeKolonne, int idInn) {

		verdi = -1;
		maksVerdi = maksVerdiInn;

		id = idInn;

		tilhorendeSudokuGruppe = new SudokuGruppe[3];
		tilhorendeSudokuGruppe[BOKS] = tilhorendeBoks;
		tilhorendeSudokuGruppe[RAD] = tilhorendeRad;
		tilhorendeSudokuGruppe[KOLONNE] = tilhorendeKolonne;

	}

	public Rad giTilhorendeRad() { return ((Rad)tilhorendeSudokuGruppe[RAD]); }
	public Boks giTilhorendeBoks() { return ((Boks)tilhorendeSudokuGruppe[BOKS]); }
	public Kolonne giTilhorendeKolonne() { return ((Kolonne)tilhorendeSudokuGruppe[KOLONNE]);}
	public SudokuGruppe giTilhorendeSudokuGruppe(int idx) {
		return (tilhorendeSudokuGruppe[idx]);
	}

	public int giId() { return(id); }

	public boolean erVerdiTillatt(int verdiInn) {

		if (verdi != -1) return(false);

		if (!tilhorendeSudokuGruppe[BOKS].erVerdiTillatt(verdiInn)) return(false);
		if (!tilhorendeSudokuGruppe[RAD].erVerdiTillatt(verdiInn)) return(false);
		if (!tilhorendeSudokuGruppe[KOLONNE].erVerdiTillatt(verdiInn)) return(false);

		return(true);
	}

	private boolean settVerdi(int verdiInn, boolean tving) {
		// Kan ikke sette en verdi hvis det allerede er satt en verdi.
		if (verdi != -1)
			return(false);

		if (verdiInn < 0) {
			throw new IllegalArgumentException("Rute: Prøvde å sette inn negativ verdi!");
		}

		if (verdiInn > maksVerdi) {
			throw new IllegalArgumentException("Rute: Prøvde å sette inn for høy verdi!");
		}

		if (!tving && !erVerdiTillatt(verdiInn))
				return(false);

		verdi = verdiInn;

		for (SudokuGruppe g: tilhorendeSudokuGruppe)
			g.brukVerdi(verdiInn, tving);

		return(true);
	}

	public void settVerdiUsikkert(int verdiInn) {
		verdi = verdiInn;

		for (SudokuGruppe g: tilhorendeSudokuGruppe)
			g.brukVerdi(verdiInn, false);
	}

	public boolean settVerdi(int verdiInn) {
		return (settVerdi(verdiInn, false));
	}

	public boolean tvingSettVerdi(int verdiInn) {
		return (settVerdi(verdiInn, true));
	}

	public int giVerdi() {
		return(verdi);
	}

	public boolean harVerdi() {
		return (verdi != -1);
	}

	public boolean fjernVerdi() {
		if (!harVerdi()) return(false);

		for (SudokuGruppe g: tilhorendeSudokuGruppe)
			g.frigiVerdi(verdi);
		
		verdi = -1;
		return(true);
	}

	public char giPrintbarVerdi() {
		// "Ingen verdi" blir representert som .
		if (verdi == -1)
			return('.');

		// Tall blir representert som tallet.
		if (verdi < 10)
			return((char)('0' + verdi));

		// verdier x: 10 <= x < 36 blir representert som A-Z
		if (verdi < 36) {
			return ((char)('A' + verdi - 10));
		}

		// verdi #36 er @ (kilde: info_om_filene)
		if (verdi == 36) {
			return ('@');
		}

		return('?');
	}

	public static boolean erPrintbarVerdi(char printbarVerdi) {
		return ((printbarVerdi > '0' && printbarVerdi <= '9')
			|| (printbarVerdi >= 'A' && printbarVerdi <= 'Z') ||
			(printbarVerdi == '@'));
	}

	public boolean settFraPrintbarVerdi(char printbarVerdi, boolean tving) {
		if (printbarVerdi == '.')
			return(fjernVerdi());

		if (printbarVerdi > '0' && printbarVerdi <= '9')
			return(settVerdi((int)(printbarVerdi - '0'), tving));

		if (printbarVerdi >= 'A' && printbarVerdi <= 'Z')
			return(settVerdi((int)(printbarVerdi - 'A' + 10), tving));

		if (printbarVerdi == '@')
			return(settVerdi(36, tving));

		throw new IllegalArgumentException("Kan ikke tolke printbar verdi.");
	}

	public long fyllUtRestenAvBrettet(int dybde, int maksverdi, Brett brettet,
		SudokuBeholder losninger) {
		// Siden optimaliseringen kutter av grener fra spilltreet, så
		// har den størst innvirkning når vi begynner. Den tar også noe
		// tid å gjennomføre, så når vi er langt nede i rekursjonen, er
		// det ikke verdt det. Denne parameteren spesifiserer når det ikke
		// er "verdt det", og den ble funnet empirisk.
		final int optDybde = (maksverdi * 3)/4;

		if (harVerdi()) {
			return(brettet.gaaTilNeste(dybde, maksverdi, brettet, losninger));
		}

		// Antall brett vi fant ved å rekursere gjennom alle tillatte verdier.
		long antBrett = 0;
		long antBrettLokal = 0;

		// Her er "lovlige" en bitmask der xte bit er 1 hvis x er tillatt, 
		// ellers 0.
		long lovlige = tilhorendeSudokuGruppe[BOKS].giRepresentasjon()
			& tilhorendeSudokuGruppe[RAD].giRepresentasjon()
			& tilhorendeSudokuGruppe[KOLONNE].giRepresentasjon();

		while (lovlige != 0) {

			// Finn en lovlig verdi og bruk den. Fjern den så fra bitmasken.
			// Dette gjør at vi bare sjekker de verdiene som faktisk er lovlige,
			// isteden for å iterere gjennom alle av dem.
			int i = (int)(Long.numberOfTrailingZeros(lovlige));
			lovlige &= ~(1L << i);
			
			if (!settVerdi(i)) continue;

			// Hvis dybden er større enn maksdybden der vi kjører
			// optimaliseringen, eller optimaliseringen sier at det
			// ikke er noen selvmotsigelse, fortsett.

			if (dybde >= optDybde || brettet.optimalisere(dybde+1)) {
				antBrettLokal = brettet.gaaTilNeste(dybde, maksverdi, brettet, 
					losninger);
				if (antBrettLokal < 0)
					return(-1);
				antBrett += antBrettLokal;
			}

			if (dybde < optDybde)
				brettet.avoptimalisere(dybde+1);

			fjernVerdi();
		}

		return(antBrett);
	}

	// Tell antall lovlige verdier i O(1) vha noe særdeles fæl bitfikling.
	public long giAntallLovligeVerdier() {

		// Loop unrolling siden javac ikke er lur nok til å gjøre det selv.
		// For forklaring av bitmasken, se fyllUtRestenAvBrettet.
		long i = tilhorendeSudokuGruppe[BOKS].giRepresentasjon()
			& tilhorendeSudokuGruppe[RAD].giRepresentasjon()
			& tilhorendeSudokuGruppe[KOLONNE].giRepresentasjon();

		// Nå trenger vi bare å telle antall bits.
		// You are not expected to understand this.

		i = i - ((i >> 1) & 0x5555555555555555L);
    	i = (i & 0x3333333333333333L) + ((i >> 2) & 0x3333333333333333L);
    	return (((i + (i >> 4)) & 0xF0F0F0F0F0F0F0FL) * 0x101010101010101L) >> 56;
	}
}

class UtfyltRute extends Rute {

	UtfyltRute(int maksVerdiInn, SudokuGruppe tilhorendeBoks, 
		SudokuGruppe tilhorendeRad, SudokuGruppe tilhorendeKolonne,
		char printbarVerdiInn, int idInn) {
		super(maksVerdiInn, tilhorendeBoks, tilhorendeRad, tilhorendeKolonne, idInn);
		super.settFraPrintbarVerdi(printbarVerdiInn, true);
	}

	public boolean fjernVerdi() {
		throw new IllegalArgumentException("Kan ikke fjerne verdi fra utfylt rute!");
	}

	public boolean settVerdi(int verdiInn) {
		throw new IllegalArgumentException("Kan ikke sette ny verdi i utfylt rute!");
	}

	public boolean settFraPrintbarVerdi(char printbarVerdiInn) {
		throw new IllegalArgumentException("Kan ikke sette ny verdi i utfylt rute!");
	}
}

class TomRute extends Rute {

	TomRute(int maksVerdiInn, SudokuGruppe tilhorendeBoks, 
		SudokuGruppe tilhorendeRad, SudokuGruppe tilhorendeKolonne,
		int idInn) {
		// Super duper, beams are gonna blind me...
		super(maksVerdiInn, tilhorendeBoks, tilhorendeRad, tilhorendeKolonne,
			idInn);
	}
}