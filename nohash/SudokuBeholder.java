// Vi lagrer brettene i serialisert form (dvs som en string). På den måten 
// slipper vi å måtte dille med deep copying inni Brett-klassen.

import java.util.ArrayList;

class SudokuBeholder implements Iterable<Brett> {
	private ArrayList<String> losninger;
	int maksAntallLosninger;
	volatile long antallTotaleLosningerFunnet;

	public SudokuBeholder(int antallLosninger) {
		losninger = new ArrayList<String>(antallLosninger);
		maksAntallLosninger = antallLosninger;
	}

	// Returnerer false hvis klassen er full.
	public boolean settInn(Brett leggTil) {
		++antallTotaleLosningerFunnet;

		if (losninger.size() >= maksAntallLosninger)
			return(false);

		losninger.add(SudokuSerialisering.skrivBrettTilUtvidetString(leggTil));
		return(true);
	}

	public void incAntallLosninger(long x) {
		antallTotaleLosningerFunnet += x;
	}

	public Brett taUt(int index) {
		return (SudokuSerialisering.lesBrettFraUtvidetString(losninger.get(index)));
	}

	// Denne gir antall lagrede løsninger, ikke antall løsninger som ble forsøkt
	// satt inn.
	public int hentAntallLosninger() {
		return(losninger.size());
	}

	public long hentAntallTotaleLosninger() {
		return(antallTotaleLosningerFunnet);
	}

	public boolean erFull() {
		return(losninger.size() == maksAntallLosninger);
	}

	// En iterator for å få alle løsningene.
	public java.util.Iterator<Brett> iterator() {
		class denneIterator implements java.util.Iterator<Brett> {
				private int index;

				denneIterator() {
					index = -1;
				}

				public boolean hasNext() {
					return (index < (hentAntallLosninger()-1));
				}
				public Brett next() {
					++index;
					return(taUt(index));
				}

				public void remove() {
					if (index == -1)
						throw new IllegalStateException();
					
					losninger.remove(index);
				}
			}

		return (new denneIterator());
	}
}
