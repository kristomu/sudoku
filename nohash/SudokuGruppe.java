import java.util.HashSet;
import java.util.Iterator;

class SudokuGruppe implements Iterable<Rute> {
	private int[] brukteTall;			// brukteTall[i] er antall ruter 
										// som har verdien. Den er int slik at vi kan
										// representere "umulige" brett.

	private int fraX, fraY, tilX, tilY;

	// Brukes av Strategi for å gjennomføre "one square rule"-eliminasjon.
	private HashSet<Rute> tilhorendeRuter;

	// Denne brukes til å forenkle giAntallLovligeVerdier i Rute. Men vi kan
	// ikke håndtere større enn 62x62-Sudokuer pga dette.
    private long ubrukteTallRepresentasjon;
    
	// Maksverdi er høyeste verdi som er tillatt i en rute.
	// fraXInn, fraYInn, tilXInn, tilYInn gir området som denne
	// gruppa inneholder, og koordinatene er inklusive (dvs fra og med, til og med)
	SudokuGruppe(int fraXInn, int fraYInn, int tilXInn, int tilYInn, int maksverdi) {

		if (maksverdi > 61)
			throw new IllegalArgumentException("Støtter ikke så store Sudokuer!");

		// Sett at alle tall er frie til å begynne med.
		brukteTall = new int[maksverdi+1];

		for (int i = 0; i < brukteTall.length; ++i)
			brukteTall[i] = 0;

		settStorrelse(fraXInn, fraYInn, tilXInn, tilYInn);

		tilhorendeRuter = new HashSet<Rute>();

		// -1 for å få bare 1ere, -1 fordi 0 ikke er tillatt.
        ubrukteTallRepresentasjon = (1L << (maksverdi+1)) - 2; 
	}

	protected void settStorrelse(int fraXInn, int fraYInn, int tilXInn, int tilYInn) {
		// Kopier over hjørnekoordinater
		fraX = fraXInn;
		fraY = fraYInn;
		tilX = tilXInn;
		tilY = tilYInn;
	}

	public long giRepresentasjon() {
        return (ubrukteTallRepresentasjon);
    }

	public int giXLengde() { return (tilX - fraX + 1); }
	public int giYLengde() { return (tilY - fraY + 1); }

	// true hvis ruta tilhører denne gruppa, false hvis ikke.
	public boolean tilhorerRuteDenne(int ruteX, int ruteY) {
		return (ruteX >= fraX && ruteX <= tilX && ruteY >= fraY && ruteY <= tilY);
	}

	public void erstattRute(Rute erstatt, Rute med) {
		if (!erRuteLagtTil(erstatt)) return;

		tilhorendeRuter.remove(erstatt);
		tilhorendeRuter.add(med);
	}

	public void leggTilRute(Rute leggTil) {
		tilhorendeRuter.add(leggTil);
	}

	// True hvis ruta er lagt til.
	public boolean erRuteLagtTil(Rute rute) {
		return (tilhorendeRuter.contains(rute));
	}

	public Iterator<Rute> iterator() {
		return (tilhorendeRuter.iterator());
	}

	// ADVARSEL: Ingen feilsjekking her.
	// Returnerer true hvis tallet allerede er brukt, ellers false.
	public boolean erVerdiTillatt(int verdi) {
		return(brukteTall[verdi] == 0);
	}

	// Returnerer true hvis det er noe galt et sted (flere ruter har samme verdi).
	public boolean erVerdiFeil(int verdi) {
		return (brukteTall[verdi] > 1);
	}

	public boolean finsFeil() {
		for (int i = 1; i < brukteTall.length; ++i)
			if (erVerdiFeil(i))
				return(true);

		return(false);
	}

	// "brukte" verdier er de som ikke er tillatte.
	public HashSet<Integer> giBrukteVerdier() {
		HashSet<Integer> brukte = new HashSet<Integer>();
		for (int i = 1; i < brukteTall.length; ++i)
			if (!erVerdiTillatt(i))
				brukte.add(i);

		return(brukte);
	}

	// Hvis tving er true, så sett verdien selv om den allerede er brukt.
	// Hvis ikke, vil denne gi en exception hvis man prøver seg på det.
	public void brukVerdi(int verdi, boolean tving) {
		if (!erVerdiTillatt(verdi) && !tving)
			throw new IllegalStateException("Prøvde å bruke en verdi som ikke er tillatt: " + verdi);

		brukteTall[verdi]++;

		ubrukteTallRepresentasjon &= ~(1L << verdi);
	}

	public void frigiVerdi(int verdi) {
		if (erVerdiTillatt(verdi))
			throw new IllegalStateException("Prøvde å frigi en verdi som ikke er brukt");

		brukteTall[verdi]--;

		if (brukteTall[verdi] == 0)
			ubrukteTallRepresentasjon |= (1L << verdi);
	}
}

class Rad extends SudokuGruppe {
	Rad(int radNr, int maksverdi) {
		// Fra (0, radNr) til og med (maksverdi-1, radNr), dvs fra venstre mot høyre

		super(0, radNr, maksverdi-1, radNr, maksverdi);
	}
}

class Kolonne extends SudokuGruppe {
	Kolonne(int kolonneNr, int maksverdi) {
		// Fra (kolonneNr, 0) til og med (kolonneNr, maksverdi-1), dvs fra toppen mot 
		// bunnen.
		super(kolonneNr, 0, kolonneNr, maksverdi-1, maksverdi);
	}
}

class Boks extends SudokuGruppe {

	Boks(int boksNr, int xLengde, int yLengde) {

		// Sett først størrelse 0. Vi finner ut størrelsen senere.
		super(0, 0, 0, 0, xLengde * yLengde);

		// Java er veldig lur. Den klager over at super ikke er første
		// statement selv når det som står over bare definerer en konstant.
		final int maksverdi = xLengde * yLengde;

		// Finn nå ut x og y-startposisjon til denne boksen. Det gjør vi
		// ved å telle til høyre, og hver gang vi kommer utenfor selve
		// Sudoku-brettet går vi helt til venstre og en boks ned 
		// (sammenlignbart med å trykke linjeskift på en skrivemaskin :-)

		int brettXLengde = maksverdi;

		int boksXStart = 0, boksYStart = 0;

		for (int i = 0; i < boksNr; ++i) {
			if (boksXStart + xLengde >= brettXLengde) {
				boksYStart += yLengde;
				boksXStart = 0;
			} else
				boksXStart += xLengde;
		}

		settStorrelse(boksXStart, boksYStart, boksXStart+xLengde-1, 
			boksYStart+yLengde-1);
	}
}
